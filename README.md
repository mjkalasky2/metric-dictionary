# Metric Dictionary

This project is used to redirect https://gitlab-org.gitlab.io/growth/product-intelligence/metric-dictionary to https://gitlab.com/gitlab-org/analytics-section/product-intelligence/metric-dictionary

See [related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/369004).

The development of [Metrics Dictionary](https://metrics.gitlab.com/) is done under [`analytics-section/metric-dictionary`](https://gitlab.com/gitlab-org/analytics-section/product-intelligence/metric-dictionary)

No issues or merge request should be created here. 
